//
//  ProductItemCell.swift
//  Mini Style Theory
//
//  Created by Dary Hilmy Iswara on 23/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit
import SDWebImage

class ProductItemCell: UICollectionViewCell {

    @IBOutlet weak var ivProduct: UIImageView!
    @IBOutlet weak var labelAvailable: UILabel!
    @IBOutlet weak var ivLoved: UIButton!
    
    var actionBlock:(()-> Void)? = nil
    
    func setData(data: Product.Data){
        let image = data.images![0]
        ivProduct.sd_setImage(with: URL(string: image))
        setLoved(loved: data.loved)
        labelAvailable.isHidden = data.isAvailable!
    }
    
    @IBAction func clickFav(_ sender: UIButton) {
        actionBlock?()
    }
    
    func setLoved(loved:Bool){
        if(loved){
            ivLoved.setImage(UIImage(named: "ic_loved"), for: .normal)
        }else{
            ivLoved.setImage(UIImage(named: "ic_not_loved"), for: .normal)
        }
    }

}
