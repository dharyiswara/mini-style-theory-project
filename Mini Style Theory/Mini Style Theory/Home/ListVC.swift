//
//  HomeVC.swift
//  Mini Style Theory
//
//  Created by Dary Hilmy Iswara on 23/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import UIKit

class ListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    private var data = [Product.Data]()
    
    private var width = 200.0
    private var margin = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        toolbar.barTintColor = UIColor.white
        setupCollectionView()
    }
    
    private func setupCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ProductItemCell", bundle: nil), forCellWithReuseIdentifier: "ProductItemCell")
    }
    
    @IBAction func clickFav(_ sender:UIImage){
        
    }

    // MARK: COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductItemCell", for: indexPath) as! ProductItemCell
//        cell.setData(data: self.data[indexPath.row], loved: false)
        cell.actionBlock = {
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = calculateWidth()
        let height = CGFloat(400.0)
        return CGSize(width: width, height: height)
    }
    
    private func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(self.width)
        let cellCount = floor(CGFloat(self.view.frame.size.width) / estimatedWidth)
        
        let marginSize = CGFloat(margin * 2)
        
        let width = (self.view.frame.size.width - CGFloat(margin) * (cellCount - 1) - marginSize) / cellCount
        
        return width
    }

}
