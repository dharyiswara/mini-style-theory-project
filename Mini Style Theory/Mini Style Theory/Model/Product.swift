//
//  Product.swift
//  Mini Style Theory
//
//  Created by Dary Hilmy Iswara on 23/10/19.
//  Copyright © 2019 Dary Hilmy Iswara. All rights reserved.
//

import Foundation
import ObjectMapper

class Product : Mappable {
    
    var data : [Data]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
    class Data : Mappable {
        var name : String?
        var isAvailable : Bool?
        var images : [String]?
        var description : String?
        var loved : Bool = false
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            name <- map["name"]
            isAvailable <- map["is_available"]
            images <- map["images"]
            description <- map["description"]
        }
        
    }
}
